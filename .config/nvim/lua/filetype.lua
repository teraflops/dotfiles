-- Archivo: ~/.config/nvim/lua/filetype.lua

-- Asocia archivos específicos con el tipo de archivo 'ansible'
vim.api.nvim_create_autocmd("BufRead,BufNewFile", {
	pattern = { "*.yml", "*.yaml" },
	callback = function()
		local is_ansible = false
		local cwd = vim.fn.getcwd()
		-- Puedes agregar más patrones o condiciones según la estructura de tu proyecto
		if vim.fn.glob(cwd .. "/ansible.cfg") ~= "" or
				vim.fn.glob(cwd .. "/playbook.yml") ~= "" then
			is_ansible = true
		end

		if is_ansible then
			vim.bo.filetype = "ansible"
		else
			-- Opcional: asignar 'yaml' si no es Ansible
			vim.bo.filetype = "yaml"
		end
	end,
})

