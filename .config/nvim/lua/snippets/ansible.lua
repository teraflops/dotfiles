-- Archivo: lua/snippets/ansible.lua

local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local t = ls.text_node
local fmt = require("luasnip.extras.fmt").fmt

return {
	-- Snippet para una tarea básica de Ansible
	s("task", fmt([[
        - name: {description}
          {module}: {parameters}
          when: {condition}
    ]], {
		description = i(1, "Descripción de la tarea"),
		module = i(2, "módulo"),
		parameters = i(3, "parámetros"),
		condition = i(4, "condición"),
	})),

	-- Snippet para definir variables
	s("var", fmt([[
        vars:
          {var_name}: {var_value}
    ]], {
		var_name = i(1, "nombre_variable"),
		var_value = i(2, "valor"),
	})),

	-- Snippet para un handler
	s("handler", fmt([[
        - name: {handler_name}
          {module}: {parameters}
    ]], {
		handler_name = i(1, "Nombre del handler"),
		module = i(2, "módulo"),
		parameters = i(3, "parámetros"),
	})),

	-- Snippet para incluir roles
	s("role", fmt([[
        roles:
          - {role_name}
    ]], {
		role_name = i(1, "nombre_role"),
	})),
}

