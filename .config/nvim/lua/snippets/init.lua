-- Archivo: ~/.config/nvim/lua/snippets/init.lua

local luasnip = require("luasnip")

-- Cargar snippets personalizados
require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/lua/snippets/" })
