-- Archivo: ~/.config/nvim/lua/plugins/ansible.lua

return {
	{
		"pearofducks/ansible-vim",
		ft = { "ansible", "yaml.ansible" }, -- Cargar para tipos de archivo 'ansible' y 'yaml.ansible'
		config = function()
			-- Configuraciones adicionales para ansible-vim (si es necesario)
			-- Por ejemplo, puedes habilitar funciones específicas o personalizar el resaltado
			vim.g.ansible_vim_highlight_extra_tasks = 1
		end,
	},
}
