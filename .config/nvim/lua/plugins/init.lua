-- Archivo: ~/.config/nvim/init.lua

-- 1. Cargar configuraciones de tipos de archivo antes de plugins
pcall(require, 'filetype') -- Ignorar errores si filetype.lua no existe

-- 2. Definir la lista principal de plugins
local plugins = {
  "tpope/vim-sleuth", -- Detecta automáticamente tabstop y shiftwidth

  -- "gc" para comentar regiones/lineas visuales
  { "numToStr/Comment.nvim", opts = {} },

  { -- Añade signos relacionados con Git al gutter, además de utilidades para gestionar cambios
    "lewis6991/gitsigns.nvim",
    opts = {},
  },

  {
    -- Si quieres ver qué esquemas de colores ya están instalados, puedes usar `:Telescope colorscheme`.
    "catppuccin/nvim",
    priority = 1000, -- Asegúrate de cargar esto antes que otros plugins de inicio.
    init = function()
      -- Cargar el esquema de colores aquí.
      vim.cmd.colorscheme("catppuccin-mocha")

      -- Puedes configurar highlights de la siguiente manera:
      vim.cmd.hi("Comment gui=none")
    end,
  },

  -- Resalta TODOs, notas, etc. en comentarios
  {
    "folke/todo-comments.nvim",
    event = "VimEnter",
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = { signs = false },
  },

  {
    "nvim-lualine/lualine.nvim",
    opts = {
      options = {
        disabled_filetypes = { "NERDTree", "NvimTree_1" },
      },
    },
  },
  { "vimwiki/vimwiki" },
  {
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup()
    end,
  }, -- Coloriza colores hexadecimales
  {
    "f-person/git-blame.nvim",
    config = function()
      require("gitblame").setup({ enabled = true })
    end,
  },
  {
    "lambdalisue/suda.vim",
  },
  { "windwp/nvim-ts-autotag" },
  -- Lazy.nvim
  {
    "hiasr/vim-zellij-navigator.nvim",
    config = function()
      require("vim-zellij-navigator").setup()
    end,
  },

  {
    "danymat/neogen",
    config = function()
      local neogen = require("neogen")

      neogen.setup({
        snippet_engine = "luasnip",
      })

      local opts = { noremap = true, silent = true }
      vim.keymap.set("n", "<leader>nc", function()
        neogen.generate({ snippet_engine = "luasnip" })
      end, opts)
    end,
  },
}

-- 3. Cargar los plugins de LSP desde 'lua/plugins/lsp.lua'
local ok, lsp_plugins = pcall(require, 'plugins.lsp')
if not ok then
  vim.notify("Error al cargar 'plugins.lsp': " .. lsp_plugins, vim.log.levels.ERROR)
  lsp_plugins = {}
end

-- 4. Verificar que lsp_plugins sea una tabla
if type(lsp_plugins) ~= 'table' then
  vim.notify("'plugins.lsp' no retorna una tabla. Verifica su configuración.", vim.log.levels.ERROR)
  lsp_plugins = {}
end

-- 5. Combinar ambas listas de plugins
vim.list_extend(plugins, lsp_plugins)

-- 6. Cargar configuraciones adicionales de plugins (como ansible.lua)
local ok_ansible, ansible_plugins = pcall(require, 'plugins.ansible')
if not ok_ansible then
  vim.notify("Error al cargar 'plugins.ansible': " .. ansible_plugins, vim.log.levels.ERROR)
  ansible_plugins = {}
end

if type(ansible_plugins) ~= 'table' then
  vim.notify("'plugins.ansible' no retorna una tabla. Verifica su configuración.", vim.log.levels.ERROR)
  ansible_plugins = {}
end

vim.list_extend(plugins, ansible_plugins)

-- 7. Cargar configuraciones adicionales de plugins (como copilot.lua)
local ok_copilot, copilot_plugins = pcall(require, 'plugins.copilot')
if not ok_copilot then
  vim.notify("Error al cargar 'plugins.copilot': " .. copilot_plugins, vim.log.levels.ERROR)
  copilot_plugins = {}
end

if type(copilot_plugins) ~= 'table' then
  vim.notify("'plugins.copilot' no retorna una tabla. Verifica su configuración.", vim.log.levels.ERROR)
  copilot_plugins = {}
end

vim.list_extend(plugins, copilot_plugins)

-- 8. Cargar configuraciones adicionales de plugins (como nvim-cmp.lua) [Opcional]
-- Si has separado la configuración de nvim-cmp, cárgala aquí
local ok_cmp, cmp_plugins = pcall(require, 'plugins.nvim-cmp')
if not ok_cmp then
  vim.notify("Error al cargar 'plugins.nvim-cmp': " .. cmp_plugins, vim.log.levels.ERROR)
  cmp_plugins = {}
end

if type(cmp_plugins) ~= 'table' then
  vim.notify("'plugins.nvim-cmp' no retorna una tabla. Verifica su configuración.", vim.log.levels.ERROR)
  cmp_plugins = {}
end

vim.list_extend(plugins, cmp_plugins)

-- 9. Retornar la lista combinada para Lazy.nvim
return plugins
