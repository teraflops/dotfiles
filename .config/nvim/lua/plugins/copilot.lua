-- Archivo: lua/plugins/copilot.lua

return {
	-- Plugin de copilot.lua
	{
		"zbirenbaum/copilot.lua",
		event = "InsertEnter",
		config = function()
			require("copilot").setup({
				panel = { enabled = false }, -- Desactiva el panel flotante de Copilot
				suggestion = { enabled = true }, -- Habilita las sugerencias integradas de Copilot
				filetypes = {
					-- Habilita Copilot para todos los tipos de archivo excepto los siguientes
					["*"] = true,
					markdown = false,
					help = false,
					gitcommit = false,
					gitrebase = false,
					hgcommit = false,
					svn = false,
					cvs = false,
					["."] = false,
				},
			})
		end,
	},

	-- Plugin de integración con nvim-cmp para copilot.lua
	{
		"zbirenbaum/copilot-cmp",
		dependencies = { "zbirenbaum/copilot.lua" },
		config = function()
			require("copilot_cmp").setup()
		end,
	},
}
