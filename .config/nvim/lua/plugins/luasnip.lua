-- Archivo: lua/plugins/luasnip.lua

return {
	{
		"L3MON4D3/LuaSnip",
		dependencies = { "rafamadriz/friendly-snippets" }, -- Colección de snippets predefinidos
		config = function()
			require("luasnip").config.set_config({
				history = true,
				updateevents = "TextChanged,TextChangedI",
			})

			-- Cargar snippets de friendly-snippets
			require("luasnip.loaders.from_vscode").lazy_load()

			-- Cargar tus snippets personalizados
			--			require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/lua/snippets/" })
		end,
	},
}
