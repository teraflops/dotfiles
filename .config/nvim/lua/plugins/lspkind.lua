-- Archivo: ~/.config/nvim/lua/plugins/lspkind.lua

return {
	{
		"onsails/lspkind-nvim",
		config = function()
			require("lspkind").init({
				preset = "default",
			})
		end,
	},
}
