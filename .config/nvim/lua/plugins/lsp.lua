-- Archivo: ~/.config/nvim/lua/plugins/lsp.lua

return {
    { -- Configuración de LSP y Plugins
        "neovim/nvim-lspconfig",
        dependencies = {
            -- Instala automáticamente LSPs y herramientas relacionadas en stdpath para Neovim
            { "williamboman/mason.nvim", config = true }, -- Debe cargarse antes que los dependientes
            "williamboman/mason-lspconfig.nvim",
            "WhoIsSethDaniel/mason-tool-installer.nvim",

            -- Actualizaciones de estado útiles para LSP.
            -- `opts = {}` es equivalente a llamar `require('fidget').setup({})`
            {
                "j-hui/fidget.nvim",
                opts = {}
            },

            -- `neodev` configura Lua LSP para tu configuración de Neovim, runtime y plugins
            -- utilizados para autocompletado, anotaciones y firmas de las APIs de Neovim
            { "folke/neodev.nvim",       opts = {} },
        },
        config = function()
            -- 1. Definir la tabla de servidores con sus configuraciones específicas
            local servers = {
                -- Configuración para Lua
                lua_ls = {
                    settings = {
                        Lua = {
                            completion = {
                                callSnippet = "Replace",
                            },
                            workspace = {
                                library = vim.api.nvim_get_runtime_file("", true),
                            },
                            diagnostics = { disable = { "missing-fields" } },
                        },
                    },
                },

                -- **Configuración para Ansible Language Server (ansiblels)**
                ansiblels = {
                    filetypes = { "ansible", "yaml.ansible" }, -- Asociar 'ansiblels' con los tipos de archivo 'ansible' y 'yaml.ansible'
                    settings = {
                        ansible = {
                            ansible = {
                                path = "ansible" -- Ruta al ejecutable de ansible
                            },
                            executionEnvironment = {
                                enabled = false -- Habilita o deshabilita el entorno de ejecución
                            },
                            python = {
                                interpreterPath = "python" -- Ruta al intérprete de Python
                            },
                            validation = {
                                enabled = true,           -- Habilita la validación
                                lint = {
                                    enabled = true,       -- Habilita el linting
                                    path = "ansible-lint" -- Ruta al ejecutable de ansible-lint
                                }
                            }
                        }
                    }
                },
            }

            -- 2. Definir capacidades extendidas (nvim-cmp)
            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

            -- 3. Configurar Mason
            require("mason").setup()

            -- 4. Configurar Mason-LSPConfig
            require("mason-lspconfig").setup({
                ensure_installed = vim.tbl_keys(servers), -- Esto incluye 'ansiblels' si está en 'servers'
                automatic_installation = true,
            })

            -- 5. Configurar Mason-Tool-Installer
            local ensure_installed = vim.tbl_keys(servers or {})
            vim.list_extend(ensure_installed, {
                "stylua",                  -- Usado para formatear código Lua
                "ansible-language-server", -- Asegurar que el servidor de Ansible esté instalado
                "ansible-lint",            -- Asegurar que ansible-lint esté instalado
            })
            require("mason-tool-installer").setup({ ensure_installed = ensure_installed })

            -- 6. Configurar los servidores con sus respectivas configuraciones
            require("mason-lspconfig").setup_handlers({
                function(server_name)
                    local server = servers[server_name] or {}
                    server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
                    require("lspconfig")[server_name].setup(server)
                end,
            })

            -- 7. Configurar Autocomandos y Mapeos de Teclas
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
                callback = function(event)
                    -- Función de mapeo simplificada
                    local map = function(keys, func, desc)
                        vim.keymap.set("n", keys, func, { buffer = event.buf, desc = "LSP: " .. desc })
                    end

                    -- Mapeos LSP con Telescope
                    map("gd", require("telescope.builtin").lsp_definitions, "[G]oto [D]efinition")
                    map("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
                    map("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation")
                    map("<leader>D", require("telescope.builtin").lsp_type_definitions, "Type [D]efinition")
                    map("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")
                    map("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
                    map("<leader>rr", vim.lsp.buf.rename, "[R]ename")
                    map("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")
                    map("K", vim.lsp.buf.hover, "Hover Documentation")
                    map("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")

                    -- Highlight References
                    local client = vim.lsp.get_client_by_id(event.data.client_id)
                    if client and client.server_capabilities.documentHighlightProvider then
                        local highlight_augroup = vim.api.nvim_create_augroup("kickstart-lsp-highlight",
                            { clear = false })
                        vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                            buffer = event.buf,
                            group = highlight_augroup,
                            callback = vim.lsp.buf.document_highlight,
                        })

                        vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
                            buffer = event.buf,
                            group = highlight_augroup,
                            callback = vim.lsp.buf.clear_references,
                        })

                        vim.api.nvim_create_autocmd("LspDetach", {
                            group = vim.api.nvim_create_augroup("kickstart-lsp-detach", { clear = true }),
                            callback = function(event2)
                                vim.lsp.buf.clear_references()
                                vim.api.nvim_clear_autocmds({ group = "kickstart-lsp-highlight", buffer = event2.buf })
                            end,
                        })
                    end

                    -- Inlay Hints (si el servidor lo soporta)
                    if client and client.server_capabilities.inlayHintProvider and vim.lsp.inlay_hint then
                        map("<leader>th", function()
                            vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
                        end, "[T]oggle Inlay [H]ints")
                    end
                end,
            })
        end,
    },
}
