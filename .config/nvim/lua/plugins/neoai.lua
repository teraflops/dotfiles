-- ~/.config/nvim/lua/plugins/neoai.lua

return {
	"Bryley/neoai.nvim",
	dependencies = {
		"MunifTanjim/nui.nvim",
	},
	lazy = false, -- Fuerza la carga del plugin al iniciar Neovim
	config = function()
		require("neoai").setup({
			ui = {
				output_popup_text = "NeoAI",
				input_popup_text = "Prompt",
				width = 30, -- Porcentaje, ej. 30%
				output_popup_height = 80, -- Porcentaje, ej. 80%
				submit = "<Enter>", -- Tecla para enviar el prompt
			},
			models = {
				{
					name = "openai",
					model = "gpt-4", -- Cambiado a GPT-4
					params = nil,
				},
			},
			register_output = {
				["g"] = function(output)
					return output
				end,
				["c"] = require("neoai.utils").extract_code_snippets,
			},
			inject = {
				cutoff_width = 75,
			},
			prompts = {
				context_prompt = function(context)
					return "Hey, I'd like to provide some context for future "
					    .. "messages. Here is the code/text that I want to refer "
					    .. "to in our upcoming conversations:\n\n"
					    .. context
				end,
			},
			mappings = {
				["select_up"] = "<C-k>",
				["select_down"] = "<C-j>",
			},
			open_ai = {
				api_key = {
					env = "OPENAI_API_KEY",
					value = nil,
					-- Puedes descomentar y personalizar la función `get` si deseas obtener la clave API de otra fuente
					-- get = function()
					--     local key = vim.fn.system("pass show openai/mytestkey")
					--     key = string.gsub(key, "\n", "")
					--     return key
					-- end,
				},
			},
			shortcuts = {
				{
					name = "textify",
					key = "<leader>as",
					desc = "Fix text with AI",
					use_context = true,
					prompt = [[
                        Please rewrite the text to make it more readable, clear,
                        concise, and fix any grammatical, punctuation, or spelling
                        errors
                    ]],
					modes = { "v" },
					strip_function = nil,
				},
				{
					name = "gitcommit",
					key = "<leader>ag",
					desc = "Generate git commit message",
					use_context = false,
					prompt = function()
						return [[
                            Using the following git diff generate a concise and
                            clear git commit message, with a short title summary
                            that is 75 characters or less:
                        ]] .. vim.fn.system("git diff --cached")
					end,
					modes = { "n" },
					strip_function = nil,
				},
			},
		})
	end,
}

