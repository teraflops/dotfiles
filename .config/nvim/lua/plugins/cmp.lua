return {
  { -- Autocompletion
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      -- Snippet Engine & its associated nvim-cmp source
      {
        "L3MON4D3/LuaSnip",
        -- ... (tu configuración existente)
      },
      "saadparwaiz1/cmp_luasnip",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-path",

      -- Añadir copilot-cmp como dependencia
      {
        "zbirenbaum/copilot-cmp",
        after = { "copilot.lua" },
      },
    },
    config = function()
      -- Importa los módulos necesarios
      local cmp = require("cmp")
      local luasnip = require("luasnip")
      luasnip.config.setup({})

      -- Tus configuraciones existentes de Luasnip
      -- ...

      -- Actualizar mapeos de Luasnip si es necesario
      vim.keymap.set({ "i", "s" }, "<C-k>", function()
        if luasnip.expand_or_jumpable() then
          luasnip.expand_or_jump()
        end
      end, { silent = true })

      -- Resto de la configuración de cmp.setup()
      cmp.setup({
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        completion = { completeopt = "menu,menuone,noinsert" },

        mapping = cmp.mapping.preset.insert({
          ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
          ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
          ["<C-y>"] = cmp.mapping.confirm({ select = true }),
          ["<C-Space>"] = cmp.mapping.complete({}),
          -- Asegúrate de que no haya mapeos conflictivos
        }),

        sources = cmp.config.sources({
          { name = "copilot", group_index = 2 },
          { name = "luasnip" },
          { name = "nvim_lsp" },
          { name = "path" },
        }),

        formatting = {
          format = function(entry, vim_item)
            vim_item.kind = ({
              copilot = "[Copilot]",
              nvim_lsp = "[LSP]",
              luasnip = "[Snippet]",
              buffer = "[Buffer]",
              path = "[Path]",
            })[entry.source.name] or vim_item.kind
            return vim_item
          end,
        },
      })
    end,
  },
  {
    "hrsh7th/cmp-path",
    event = "InsertEnter", -- Carga el plugin al entrar en modo inserción
    config = function()
      -- No se requiere configuración adicional para cmp-path
      -- Puedes personalizar las opciones aquí si lo deseas
    end,
  },
}
