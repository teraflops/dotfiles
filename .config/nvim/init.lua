vim.loader.enable()

require("set")
require("remap")
require("autocmd")
require("lazy_init")
require("snippets")

-- Cargar configuración de tipos de archivo
pcall(require, 'filetype') -- Ignorar errores si filetype.lua no existe

function ReplaceWordUnderCursor()
    local word = vim.fn.expand("<cword>")
    local replace = vim.fn.input("Replace \"" .. word .. "\" with: ")
    if replace ~= "" then
        vim.cmd("%s/\\V" .. word .. "/" .. replace .. "/g")
    end
end

vim.api.nvim_set_keymap("n", "<Leader>r", ":lua ReplaceWordUnderCursor()<CR>", { noremap = true, silent = true })

function ReplacePhraseUnderCursor()
    -- Obtén la palabra o frase bajo el cursor en modo visual
    local phrase = vim.fn.input("Replace phrase: ")
    if phrase == "" then
        print("No phrase provided.")
        return
    end
    local replace = vim.fn.input("Replace \"" .. phrase .. "\" with: ")
    if replace ~= "" then
        vim.cmd("%s/\\V" .. vim.fn.escape(phrase, "/\\") .. "/" .. replace .. "/g")
    end
end

vim.api.nvim_set_keymap("n", "<Leader>f", ":lua ReplacePhraseUnderCursor()<CR>", { noremap = true, silent = true })

local cmp = require("cmp")
local luasnip = require("luasnip")

-- Opciones comunes para los mapeos
local opts = { noremap = true, silent = true }

-- Función para manejar <Tab>
local function tab_complete(fallback)
    if cmp.visible() then
        cmp.select_next_item()
    elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
    else
        fallback()
    end
end

-- Función para manejar <S-Tab>
local function s_tab_complete(fallback)
    if cmp.visible() then
        cmp.select_prev_item()
    elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
    else
        fallback()
    end
end

-- Mapeo de <Tab> en modo de inserción y selección
vim.keymap.set("i", "<Tab>", function(fallback)
    tab_complete(fallback)
end, opts)

vim.keymap.set("s", "<Tab>", function(fallback)
    tab_complete(fallback)
end, opts)

-- Mapeo de <S-Tab> en modo de inserción y selección
vim.keymap.set("i", "<S-Tab>", function(fallback)
    s_tab_complete(fallback)
end, opts)

vim.keymap.set("s", "<S-Tab>", function(fallback)
    s_tab_complete(fallback)
end, opts)
