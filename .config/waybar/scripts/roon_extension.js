const { RoonExtension } = require('roon-kit');
const fs = require('fs');
const { exec } = require('child_process');

// Ruta al script de letras en Python
const lyricsScriptPath = "/home/teraflops/.config/waybar/scripts/get_lyrics.py"; // Asegúrate de que la ruta sea correcta

// Configuración de la extensión
const extension = new RoonExtension({
    description: {
        extension_id: 'roon-kit-now-playing',
        display_name: "Roon Kit Now Playing",
        display_version: "0.1.0",
        publisher: 'roon-kit',
        email: 'stevenic@microsoft.com',
        website: 'https://github.com/Stevenic/roon-kit'
    },
    RoonApiBrowse: 'not_required',
    RoonApiImage: 'required',
    RoonApiTransport: 'required',
    subscribe_outputs: false,
    subscribe_zones: true,
    log_level: 'none'
});

// Función para limpiar el título de la canción
function cleanTitle(title) {
    return title
        .replace(/^[A-Za-z0-9]{1,3}\s+/, '') // Eliminar prefijos como "B3 "
        .replace(/[$#@!%^&*(){}\[\]<>?/\\|`~]/g, '') // Eliminar caracteres especiales
        .trim(); // Eliminar espacios adicionales
}

// Función para obtener letras usando el script de Python
function getLyrics(artist, title, callback) {
    console.log(`Obteniendo letras para: Artista - ${artist}, Título - ${title}`);
    exec(`python3 ${lyricsScriptPath} "${artist}" "${title}"`, (error, stdout, stderr) => {
        if (error) {
            console.error(`Error al obtener letras: ${stderr}`);
            callback("Letras no disponibles.");
        } else {
            const lyrics = stdout.trim();
            console.log("Letras obtenidas:", lyrics);  // Log para verificar las letras obtenidas
            callback(lyrics || "Letras no encontradas.");
        }
    });
}

// Función para manejar las actualizaciones de las zonas (cambio de canción)
extension.on("subscribe_zones", async (core, response, body) => {
    const changedZones = body.zones_changed ?? [];
    const addedZones = body.zones_added ?? [];
    const removedZones = body.zones_removed ?? [];

    if (removedZones.length > 0) {
        fs.writeFileSync('/tmp/waybar_roon_info.json', JSON.stringify({ text: '', tooltip: '' }));
        exec("/usr/bin/pkill -RTMIN+3 waybar");
    }

    for (const zone of [...addedZones, ...changedZones]) {
        if (zone.state === 'playing') {
            const track = cleanTitle(zone.now_playing?.one_line.line1 || '');
            const artist = zone.now_playing?.one_line.line2 || '';
            const displayText = `${track} - ${artist}`;

            // Obtener letras y escribir el título y letras en JSON para Waybar
            getLyrics(artist, track, (lyrics) => {
                const data = {
                    text: "🎵 " + displayText,
                    tooltip: lyrics
                };

                console.log("Escribiendo JSON para Waybar:", data);  // Log para verificar el JSON
                fs.writeFileSync('/tmp/waybar_roon_info.json', JSON.stringify(data));
                exec("/usr/bin/pkill -RTMIN+3 waybar");
            });

            // Obtener la carátula del álbum y mostrar notificación
            const image_key = zone.now_playing?.image_key;
            if (image_key) {
                try {
                    const imageData = await core.services.RoonApiImage.get_image(image_key, { width: 300, height: 300 });
                    const coverPath = '/tmp/roon_album_cover.jpg';
                    fs.writeFileSync(coverPath, imageData.image);

                    // Enviar notificación con la carátula y el nombre de la canción
                    exec(`notify-send -i ${coverPath} "Now Playing" "${displayText}"`);
                } catch (error) {
                    console.error("Error obteniendo la carátula:", error);
                }
            } else {
                exec(`notify-send "Now Playing" "${displayText}"`);
            }
        } else if (zone.state === 'paused' || zone.state === 'stopped') {
            fs.writeFileSync('/tmp/waybar_roon_info.json', JSON.stringify({ text: '', tooltip: '' }));
            exec("/usr/bin/pkill -RTMIN+3 waybar");
        }
    }
});

// Iniciar descubrimiento de core y conectar
extension.start_discovery();
extension.set_status('Esperando conexión al Core de Roon...');

// Esperar a que el core esté emparejado
(async () => {
    const core = await extension.get_core();
    extension.set_status('Emparejado con el Core de Roon');
})();

