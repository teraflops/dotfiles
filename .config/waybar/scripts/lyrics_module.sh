#!/bin/bash

# Paths and temporary files
CURRENT_TRACK_FILE="/tmp/current_track.txt"
LYRICS_FILE="/tmp/lyrics.txt"
LOG_FILE="/tmp/lyrics_error.log"
STATE_FILE="/tmp/lyrics_state.txt"

# Get current track info
TRACK_INFO=$(playerctl metadata --format "{{artist}} - {{title}}" 2>>"$LOG_FILE")

if [ -z "$TRACK_INFO" ]; then
    echo "No track info available" > "$LYRICS_FILE"
    exit 1
fi

# Extract artist and title
#ARTIST=$(echo "$TRACK_INFO" | cut -d '-' -f 1 | xargs)
#TITLE=$(echo "$TRACK_INFO" | cut -d '-' -f 2 | xargs)
ARTIST=$(echo "$TRACK_INFO" | cut -d '-' -f 1 | sed 's/^ *//;s/ *$//')
TITLE=$(echo "$TRACK_INFO" | cut -d '-' -f 2- | sed 's/^ *//;s/ *$//')
# Check if the track has changed
if [ "$TRACK_INFO" != "$(cat "$CURRENT_TRACK_FILE" 2>/dev/null)" ]; then
    echo "$TRACK_INFO" > "$CURRENT_TRACK_FILE"

    # Fetch lyrics using the Python script
    lyrics=$(python3 ~/.config/waybar/scripts/get_lyrics.py "$ARTIST" "$TITLE")

    if [[ "$lyrics" == "Letras no encontradas." ]]; then
        lyrics="Letras no encontradas para $ARTIST - $TITLE"
    fi

    echo "$lyrics" > "$LYRICS_FILE"
else
    lyrics=$(cat "$LYRICS_FILE")
fi

# Read the state
if [ -f "$STATE_FILE" ]; then
    STATE=$(cat "$STATE_FILE")
else
    STATE="full"
fi

# Adjust lyrics based on the state
if [ "$STATE" = "second_half" ]; then
    total_lines=$(echo "$lyrics" | wc -l)
    half_line=$(( (total_lines + 1) / 2 ))
    lyrics=$(echo "$lyrics" | tail -n +$half_line)
fi

# Generate JSON for Waybar
jq -c -n --arg text "🎵" --arg tooltip "$lyrics" '{"text":$text,"tooltip":$tooltip}'


