#!/bin/bash

STATE_FILE="/tmp/lyrics_state.txt"

if [ ! -f "$STATE_FILE" ]; then
    echo "second_half" > "$STATE_FILE"
else
    STATE=$(cat "$STATE_FILE")
    if [ "$STATE" = "second_half" ]; then
        echo "full" > "$STATE_FILE"
    else
        echo "second_half" > "$STATE_FILE"
    fi
fi

# Send signal to Waybar to update the module immediately
pkill -SIGRTMIN+9 waybar

