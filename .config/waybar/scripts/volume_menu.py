#!/usr/bin/env python3
from gi.repository import AppIndicator3
from gi.repository import Gtk, GLib, GObject
import gi
import subprocess
import psutil
import sys
import os

gi.require_version("Gtk", "3.0")


class VolumeSlider(Gtk.Window):
    def __init__(self):
        super().__init__(title="Control de Volumen")
        self.set_default_size(50, 300)  # Tamaño de la ventana ajustado

        # Crear el control deslizante vertical
        self.slider = Gtk.Scale(orientation=Gtk.Orientation.VERTICAL)
        self.slider.set_range(0, 100)  # Rango de 0 a 100 para el volumen
        self.slider.set_value(self.get_current_volume())
        # Invertir para que vaya de abajo hacia arriba
        self.slider.set_inverted(True)
        self.slider.connect("value-changed", self.on_volume_changed)

        # Ajustar el ancho del control deslizante
        self.slider.set_size_request(30, -1)  # Establece el ancho a 30 píxeles

        # Añadir el control deslizante a la ventana
        self.add(self.slider)

    def get_current_volume(self):
        """Obtiene el volumen actual usando pamixer"""
        result = subprocess.run(
            ["pamixer", "--get-volume"], capture_output=True, text=True)
        # Default al 50%
        return int(result.stdout.strip()) if result.returncode == 0 else 50

    def on_volume_changed(self, slider):
        """Ajusta el volumen según la posición del control deslizante"""
        volume = int(slider.get_value())
        subprocess.run(["pamixer", "--set-volume", str(volume)])


def is_another_instance_running():
    """Comprueba si hay otra instancia de este script en ejecución"""
    current_pid = os.getpid()
    for process in psutil.process_iter(attrs=['pid', 'cmdline']):
        if process.info['pid'] != current_pid and 'volume_slider.py' in process.info['cmdline']:
            return True
    return False


if __name__ == "__main__":
    if is_another_instance_running():
        print("Ya hay una instancia ejecutándose. Finalizando esta.")
        sys.exit()

    # Ejecutar la aplicación
    win = VolumeSlider()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
