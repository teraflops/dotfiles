#!/usr/bin/env python3
from gi.repository import Gtk, GLib, Gio
import threading
from pydbus import SystemBus
import subprocess


class BluetoothMenu(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="org.example.BluetoothMenu")
        self.bus = SystemBus()
        self.adapter = self.bus.get("org.bluez", "/org/bluez/hci0")
        self.object_manager = self.bus.get("org.bluez", "/")
        self.device_window = None
        self.scanning = False
        self.discovered_devices = {}

    def do_activate(self):
        self.window = Gtk.ApplicationWindow(application=self)
        self.window.set_default_size(200, 150)
        self.window.set_title("Bluetooth Menu")
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        if hasattr(self.window, "set_child"):
            self.window.set_child(self.vbox)
        else:
            self.window.add(self.vbox)

        self.build_menu()
        self.update_bluetooth_buttons()
        self.window.show()

    def build_menu(self):
        self.item_scan = Gtk.Button(label='Escanear Dispositivos')
        self.item_scan.connect('clicked', self.scan_and_show_devices)
        self.add_to_box(self.vbox, self.item_scan)

        self.item_on = Gtk.Button(label='Encender Bluetooth')
        self.item_on.connect('clicked', self.turn_on_bluetooth)
        self.add_to_box(self.vbox, self.item_on)

        self.item_off = Gtk.Button(label='Apagar Bluetooth')
        self.item_off.connect('clicked', self.turn_off_bluetooth)
        self.add_to_box(self.vbox, self.item_off)

        item_quit = Gtk.Button(label='Cerrar')
        item_quit.connect('clicked', self.exit_app)
        self.add_to_box(self.vbox, item_quit)

    def add_to_box(self, box, widget):
        if hasattr(box, "append"):
            box.append(widget)
        else:
            box.pack_start(widget, True, True, 0)

    def update_bluetooth_buttons(self):
        powered = self.adapter.Powered
        self.item_on.set_sensitive(not powered)
        self.item_off.set_sensitive(powered)

    def scan_and_show_devices(self, _):
        if not self.adapter.Discoverable:
            self.adapter.Discoverable = True
        if not self.adapter.Pairable:
            self.adapter.Pairable = True

        if not self.scanning:
            self.adapter.StartDiscovery()
            self.scanning = True
            self.show_notification(
                "Escaneo de dispositivos Bluetooth iniciado")

        self.show_device_list_window()

    def show_device_list_window(self):
        if self.device_window:
            self.device_window.destroy()

        self.device_window = Gtk.Window(title="Dispositivos Disponibles")
        self.device_window.set_default_size(400, 500)
        self.device_window.set_resizable(True)
        self.device_window.set_transient_for(self.window)

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(
            Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scrolled_window.set_min_content_height(400)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.device_list_vbox = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL, spacing=10)

        if hasattr(scrolled_window, "set_child"):
            scrolled_window.set_child(self.device_list_vbox)
        else:
            scrolled_window.add(self.device_list_vbox)

        if hasattr(vbox, "append"):
            vbox.append(scrolled_window)
        else:
            vbox.pack_start(scrolled_window, True, True, 0)

        # Botón de Refrescar
        refresh_button = Gtk.Button(label="Refrescar")
        refresh_button.connect("clicked", self.refresh_device_list)
        if hasattr(vbox, "append"):
            vbox.append(refresh_button)
        else:
            vbox.pack_start(refresh_button, False, False, 0)

        # Botón de Cerrar
        close_button = Gtk.Button(label="Cerrar")
        close_button.connect("clicked", self.stop_scan_and_close)
        if hasattr(vbox, "append"):
            vbox.append(close_button)
        else:
            vbox.pack_start(close_button, False, False, 0)

        if hasattr(self.device_window, "set_child"):
            self.device_window.set_child(vbox)
        else:
            self.device_window.add(vbox)

        self.device_window.present()

        GLib.timeout_add_seconds(2, self.update_device_list)

    def refresh_device_list(self, _):
        """Actualizar la lista de dispositivos visualmente sin detener el escaneo."""
        # Limpiar la lista de dispositivos visualmente de forma compatible
        child = self.device_list_vbox.get_first_child()
        while child:
            self.device_list_vbox.remove(child)
            child = self.device_list_vbox.get_first_child()

        # Reiniciar el diccionario para evitar dispositivos duplicados
        self.discovered_devices.clear()

        # Llamar a update_device_list para llenar la lista nuevamente
        self.update_device_list()

    def update_device_list(self):
        if not self.scanning:
            return False

        devices = self.get_discovered_devices()
        print("Actualización de dispositivos descubiertos:", devices)

        for name, address in devices:
            if address not in self.discovered_devices:
                self.discovered_devices[address] = name
                button = Gtk.Button(label=f"{name} ({address})")
                button.connect("clicked", self.pair_trust_connect, address)
                self.add_to_box(self.device_list_vbox, button)

        return True

    def stop_scan_and_close(self, _):
        if self.scanning:
            self.adapter.StopDiscovery()
            self.scanning = False
            self.show_notification(
                "Escaneo de dispositivos Bluetooth detenido")
        if self.device_window:
            self.device_window.destroy()
        self.discovered_devices.clear()

    def get_discovered_devices(self):
        devices = []
        managed_objects = self.object_manager.GetManagedObjects()

        for path, interfaces in managed_objects.items():
            if "org.bluez.Device1" in interfaces:
                device_properties = interfaces["org.bluez.Device1"]
                if device_properties.get("Paired", False) and not device_properties.get("Connected", False):
                    continue
                name = device_properties.get("Name", "Desconocido")
                address = device_properties.get("Address")
                devices.append((name, address))

        return devices

    def pair_trust_connect(self, button, address):
        threading.Thread(target=self.pair_trust_connect_thread,
                         args=(address,)).start()

    def pair_trust_connect_thread(self, address):
        device_path = f"/org/bluez/hci0/dev_{address.replace(':', '_')}"
        device = self.bus.get("org.bluez", device_path)

        try:
            device.Pair()
            device.Trusted = True
            device.Connect()
            self.show_notification(f"Conectado a {address}")
        except Exception as e:
            self.show_notification(f"Error al conectar con {address}: {e}")

    def turn_on_bluetooth(self, _):
        self.adapter.Powered = True
        self.show_notification("Bluetooth Encendido")
        self.update_bluetooth_buttons()

    def turn_off_bluetooth(self, _):
        self.adapter.Powered = False
        self.show_notification("Bluetooth Apagado")
        self.update_bluetooth_buttons()

    def exit_app(self, _):
        if self.scanning:
            self.adapter.StopDiscovery()
        self.quit()

    def show_notification(self, message):
        subprocess.run(['notify-send', message])


def main():
    app = BluetoothMenu()
    app.run()


if __name__ == "__main__":
    main()
