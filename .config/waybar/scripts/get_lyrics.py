import sys
from lyricsgenius import Genius

# Leer la clave API desde el archivo
with open("/home/teraflops/apikeys/genius", "r") as file:
    genius_token = file.read().strip()

# Inicializar la API de Genius con el token
genius = Genius(genius_token)

# Leer el nombre del artista y la canción desde argumentos
artist = sys.argv[1]
song_title = sys.argv[2]

# Buscar las letras de la canción
song = genius.search_song(song_title, artist)
if song:
    print(song.lyrics)
else:
    print("Letras no encontradas.")

