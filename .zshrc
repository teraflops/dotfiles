# If you come from bash you might have to change your $PATH.
export PATH=$HOME/.local/bin:/usr/local/bin:$PATH
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
export DEFAULT_USER=$USER
export EDITOR="nvim"
ZSH_THEME="powerlevel10k/powerlevel10k"
plugins=(
  git
  emoji
  bundler
  dotenv
)
source $ZSH/oh-my-zsh.sh
source /usr/share/doc/pkgfile/command-not-found.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


alias bp-cli="/home/teraflops/git/bepasty-client-cli/bin/bepasty-cli -p "$credentials" -u https://bepasty.priet.us "$@" | sed 's|$|/+inline|' |tail -n1 | wl-copy -n" 

# Autocompletion function for mympc in Zsh
_mympc_autocomplete() {
    _arguments -C \
        '--artist[Search for songs by artist]:artist name:->artists' \
        '--genre[Search for songs by genre]:genre name:->genres' \
        '--album[Search for songs by album]:album name:->albums' \
        '--play[Play immediately]' \
        '--save-playlist[Save the current playlist]:playlist name:->playlists' \
        '--list-artists[List all available artists]' \
        '--list-genres[List all available genres]' \
        '--list-albums[List all available albums]' \
        '--random[Activate random mode]' \
        '--repeat[Activate repeat mode]' \
        '--number[Specify the number of songs]:number of songs' \
        '-n[Specify the number of songs]:number of songs' \
        '--help[Show help message]' \
        '-h[Show help message]'

    case $state in
        artists)
            compadd -- ${(f)"$(mpc list artist)"}
            ;;
        genres)
            compadd -- ${(f)"$(mpc list genre)"}
            ;;
        albums)
            compadd -- ${(f)"$(mpc list album)"}
            ;;
        playlists)
            compadd -- ${(f)"$(mpc lsplaylists)"}
            ;;
        *)
            ;;
    esac
}

# Register the autocompletion function in Zsh
compdef _mympc_autocomplete mympc

