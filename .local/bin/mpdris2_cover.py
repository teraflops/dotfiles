import os
import requests
import urllib.parse
import asyncio
from dbus_next.aio import MessageBus
from dbus_next import Variant
from pathlib import Path

# File path to your Last.fm API key
API_KEY_FILE = "/home/teraflops/apikeys/lastfm"

def get_lastfm_api_key():
    """Read the Last.fm API key from a file."""
    try:
        with open(API_KEY_FILE, 'r') as file:
            api_key = file.readline().strip()
            print(f"Successfully read Last.fm API key from {API_KEY_FILE}")
            return api_key
    except FileNotFoundError:
        print(f"Error: API key file '{API_KEY_FILE}' not found. Please check the path.")
        return None
    except Exception as e:
        print(f"Error reading API key: {e}")
        return None

# Get the Last.fm API key
LASTFM_API_KEY = get_lastfm_api_key()
if not LASTFM_API_KEY:
    raise RuntimeError("Last.fm API key could not be loaded. Please check the API key file path.")
else:
    print("API key loaded successfully. Continuing with the script.")

# Base path for your music library
MUSIC_BASE_PATH = "/home/teraflops/media/all"

def cover_exists(album_dir):
    """Check if 'cover.jpg' or 'Cover.jpg' exists in the album directory."""
    cover_path_lower = album_dir / "cover.jpg"
    cover_path_upper = album_dir / "Cover.jpg"
    return cover_path_lower.exists() or cover_path_upper.exists()

def download_image(cover_url, output_path):
    """Download the cover image from a URL and save it to the specified path."""
    try:
        response = requests.get(cover_url, stream=True)
        if response.status_code == 200:
            with open(output_path, 'wb') as f:
                for chunk in response.iter_content(1024):
                    f.write(chunk)
            print(f"Image downloaded to {output_path}")
            return output_path
        else:
            print(f"Failed to download image. HTTP Status: {response.status_code}")
    except Exception as e:
        print(f"Error downloading image: {e}")
    return None

def get_largest_cover_image(data):
    """Select the largest cover image available in the Last.fm response."""
    if 'album' in data and 'image' in data['album']:
        # Define size priority order
        size_priority = ["mega", "extralarge", "large", "medium", "small"]
        for size in size_priority:
            for img in data['album']['image']:
                if img["size"] == size and img["#text"]:
                    print(f"Using {size} cover: {img['#text']}")
                    return img["#text"]
    return None

def download_cover_from_lastfm(artist, album, album_dir):
    """Download album cover from Last.fm and save it to the album directory."""
    try:
        print(f"Fetching cover for '{album}' by {artist} from Last.fm...")
        # Last.fm API URL
        url = f"http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key={LASTFM_API_KEY}&artist={artist}&album={album}&format=json"
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            cover_url = get_largest_cover_image(data)
            if cover_url:
                album_dir.mkdir(parents=True, exist_ok=True)  # Ensure directory exists
                local_cover_path = album_dir / "cover.jpg"  # Save as 'cover.jpg'
                download_image(cover_url, local_cover_path)
                print(f"Downloaded cover to {local_cover_path}")
                return local_cover_path
        print(f"No cover found for '{album}' by {artist} on Last.fm.")
    except Exception as e:
        print(f"Error fetching cover from Last.fm: {e}")
    return None

def get_local_album_dir(xesam_url):
    """Extract the album directory from the `xesam:url` path."""
    if not xesam_url:
        print(f"Warning: xesam:url is empty or missing. Skipping cover detection.")
        return None

    try:
        print(f"Raw xesam:url value: {xesam_url}")

        # Convert the xesam URL into a local file path
        local_path = urllib.parse.unquote(xesam_url).replace("file://", "")
        print(f"Converted local path: {local_path}")

        # Remove '/trackXXXX' if this is a CUE file path
        if "/track" in local_path and ".cue" in local_path:
            local_path = local_path.split("/track")[0]
            print(f"Stripped CUE path: {local_path}")

        # If the path is not absolute, prepend the base music directory
        track_path = Path(local_path)
        if not track_path.is_absolute():
            track_path = Path(MUSIC_BASE_PATH) / track_path
            print(f"Updated to absolute path: {track_path}")

        # Check if the resulting path exists
        if not track_path.exists():
            print(f"Warning: The path '{track_path}' does not exist.")
            return None

        # Handle CUE files: Go up to the actual album directory
        if track_path.suffix == ".cue":
            return track_path.parent

        # Standard path handling
        if track_path.is_file():
            return track_path.parent
        else:
            print(f"Path is not a valid file: {track_path}")
    except Exception as e:
        print(f"Error extracting local album directory: {e}")
    return None

class MprisCoverHandler:
    def __init__(self):
        self.bus = None

    async def on_properties_changed(self, interface, changed, invalidated):
        """Handler for MPRIS property changes."""
        if 'Metadata' in changed:
            metadata = changed['Metadata'].value

            # Extract metadata properties
            track_title = metadata.get('xesam:title', Variant('s', 'Unknown')).value
            artist = metadata.get('xesam:artist', Variant('as', [])).value
            album = metadata.get('xesam:album', Variant('s', 'Unknown')).value
            xesam_url = metadata.get('xesam:url', Variant('s', '')).value

            if not isinstance(artist, list):
                artist = [str(artist)]
            artist = [str(a) for a in artist]

            if not artist or album == 'Unknown' or not xesam_url:
                print(f"Skipping track '{track_title}' due to missing metadata.")
                return

            print(f"Now playing: {track_title} by {', '.join(artist)} from the album '{album}'")

            # Determine the local album directory based on xesam:url
            album_dir = get_local_album_dir(xesam_url)
            if not album_dir:
                print(f"Could not determine album directory for '{track_title}'. xesam:url: {xesam_url}")
                return

            # Check if a cover already exists
            if cover_exists(album_dir):
                print(f"Cover already exists in '{album_dir}', doing nothing.")
            else:
                print(f"No cover found in '{album_dir}', attempting to download.")
                download_cover_from_lastfm(artist[0], album, album_dir)

            # Delay and execute the command to refresh Waybar
            await asyncio.sleep(0.5)
            os.system("/usr/bin/pkill -RTMIN+23 waybar")

    async def main(self):
        """Main function to connect to D-Bus and listen for changes."""
        print("Starting D-Bus listener...")
        self.bus = await MessageBus().connect()
        introspectable = await self.bus.introspect("org.mpris.MediaPlayer2.mpd", "/org/mpris/MediaPlayer2")
        obj = self.bus.get_proxy_object("org.mpris.MediaPlayer2.mpd", "/org/mpris/MediaPlayer2", introspectable)
        properties = obj.get_interface("org.freedesktop.DBus.Properties")

        # Execute the command once when the listener starts
        await asyncio.sleep(0.5)  # Small delay to ensure Waybar is ready
        os.system("/usr/bin/pkill -RTMIN+23 waybar")

        properties.on_properties_changed(self.on_properties_changed)
        print("D-Bus listener started successfully.")
        await asyncio.Future()  # Run forever

# Run the main loop to monitor MPRIS changes
fetcher = MprisCoverHandler()
asyncio.run(fetcher.main())

