#!/bin/bash

wf-recorder_check() {
	if pgrep -x "wf-recorder" > /dev/null; then
			pkill -INT -x wf-recorder
      pkill -RTMIN+11 waybar
			#notify-send "Stopping all instances of wf-recorder"
			exit 0
	fi

record
}

record() {
  pkill -RTMIN+11 waybar
  screencast_dir="$HOME/Screenrecord"
  mkdir -p "$screencast_dir"
  timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
  screencast_file="$screencast_dir/screencast_$timestamp.mkv"
  wf-recorder -c h264_vaapi -d /dev/dri/by-path/pci-0000:65:00.0-render -f "$screencast_file"
  pkill -RTMIN+11 waybar
  webm_file=${screencast_file%.*}.webm

  ffmpeg -i "$screencast_file" -c:v libvpx-vp9 -lossless 1 "$webm_file"
  notify-send "video post-processed" 
}

wf-recorder_check

scp "$webm_file" teraflops@192.168.199.29:/usr/share/nginx/fiche/
paste=$(basename $webm_file)
echo -n "https://paste.priet.us/$paste" | wl-copy
rm "$screencast_file"
