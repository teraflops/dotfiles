#!/bin/bash

asusctl fan-curve -e true --mod-profile balanced
asusctl fan-curve -D 40c:15%,62c:16%,65c:18%,68c:22%,70c:26%,72c:36%,74c:44%,76c:54% -f CPU --mod-profile balanced
asusctl fan-curve -D 40c:10%,59c:20%,62c:25%,65c:29%,67c:33%,69c:41%,71c:52%,73c:62% -f GPU --mod-profile balanced
asusctl fan-curve -D 40c:2%,62c:20%,65c:22%,68c:22%,70c:36%,72c:50%,74c:73%,76c:94% -f MID --mod-profile balanced
asusctl fan-curve -e true --mod-profile balanced
