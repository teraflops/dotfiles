#!/bin/bash

sleep 5;
screenshot_dir="$HOME/Screenshots"
timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
screenshot_file="$screenshot_dir/screenshot_$timestamp.png"

# Function to select the region
select_region() {
    region=$(slurp)
    echo "$region"
}

# Function to take screenshot
take_screenshot() {
    grim -g "$1" "$screenshot_file"
    scp "$screenshot_file" teraflops@192.168.199.29:/usr/share/nginx/fiche/
    paste=$(basename $screenshot_file)
    echo -n "https://paste.priet.us/$paste" | wl-copy 
}

main() {
    region=$(select_region)
    if [ -n "$region" ]; then
        take_screenshot "$region"
    else
        echo "No region selected, exiting..."
        exit 1
    fi
}

main
