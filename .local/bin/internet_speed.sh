#!/bin/zsh

# make a timer or cronjob ans run it lets say every 10 minutes or so

speedtest=$(speedtest --accept-gdpr |grep 'Download\|Upload')
download=$(echo $speedtest| grep 'Download' | awk '{print $3,$4}')
upload=$(echo $speedtest| grep 'Upload' | awk '{print $3,$4}')
echo "⬇ $download" > /home/teraflops/.config/waybar/scripts/down.txt
echo "⬆ $upload" > /home/teraflops/.config/waybar/scripts/up.txt
pkill -RTMIN+20 waybar
pkill -RTMIN+21 waybar
