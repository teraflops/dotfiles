#!/usr/bin/env python3

import json
import os

CACHE_FILE = "/tmp/network_speed_cache.json"


def main():
    if os.path.exists(CACHE_FILE):
        try:
            with open(CACHE_FILE, "r") as f:
                data = json.load(f)
                print(json.dumps(data))
        except Exception as e:
            error_data = {
                "text": "Error",
                "tooltip": f"Error al leer el caché: {e}",
                "class": "network-speed-error"
            }
            print(json.dumps(error_data))
    else:
        error_data = {
            "text": "No data",
            "tooltip": "El caché de velocidad de red no está disponible.",
            "class": "network-speed-error"
        }
        print(json.dumps(error_data))


if __name__ == "__main__":
    main()
