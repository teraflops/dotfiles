#!/bin/bash

# Ruta a la carpeta de wallpapers
WALLPAPER_DIR=~/Wallpapers/hyprlock

# Función para cambiar el wallpaper
cambiar_wallpaper() {
    # Seleccionar un wallpaper aleatorio de la carpeta
    WALLPAPER=$(find "$WALLPAPER_DIR" -type f | shuf -n 1)

    # Cargar el wallpaper
    hyprctl hyprpaper preload "$WALLPAPER"
    hyprctl hyprpaper wallpaper "eDP-1,$WALLPAPER"
}

# Cambiar el wallpaper cada 15 minutos
while true; do
    cambiar_wallpaper
    sleep 900  # 900 segundos = 15 minutos
done

