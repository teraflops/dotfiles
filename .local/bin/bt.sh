#!/bin/bash
#
status=$(bluetoothctl show |awk  -F"Powered:" '{print $2}' | xargs)

if [[ $status == "no" ]]; then
  bluetoothctl power on
elif [[ $status == "yes" ]]; then
  bluetoothctl power off
fi
