#!/bin/bash

# Define the array of colors
colors=("ff3333" "00cc66" "3399ff" "ffff00" "ffffff")

# Read the current value from the file
current_color=$(</home/teraflops/.config/current_colour)

# Find the index of the current value in the array
index=-1
for i in "${!colors[@]}"; do
    if [[ "${colors[$i]}" == "$current_color" ]]; then
        index=$i
        break
    fi
done

# Print the next value in the array
if (( index != -1 )); then
    next_index=$(( (index + 1) % ${#colors[@]} ))
    next_color=${colors[$next_index]}
    echo "$next_color" > ~/.config/current_colour
    asusctl led-mode static -c "$next_color"
else
    echo "Current color not found in the array."
fi

