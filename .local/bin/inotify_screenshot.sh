#!/bin/bash

# Ruta del directorio a monitorear
DIRECTORIO="/home/teraflops/Imágenes/Capturas de pantalla"

# Monitorear el directorio y ejecutar el comando cuando se cree un archivo nuevo
inotifywait -m -e create --format '%f' "$DIRECTORIO" | while read NOMBRE_FICHERO
do
  # Ejecutar el comando
  /home/teraflops/.local/bin/bepasty-cli -p "$credentials" -u https://bepasty.priet.us "$DIRECTORIO/$NOMBRE_FICHERO" | sed 's|$|/+inline|' |tail -n1 | wl-copy -n
done

