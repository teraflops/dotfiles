#!/bin/bash

sleep 5;
screenshot_dir="$HOME/Screenshots"
timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
screenshot_file="$screenshot_dir/screenshot_$timestamp.png"

# Function to take screenshot
take_screenshot() {
    grim "$screenshot_file"
    scp "$screenshot_file" teraflops@192.168.199.29:/usr/share/nginx/fiche/
    paste=$(basename $screenshot_file)
    echo -n "https://paste.priet.us/$paste" | wl-copy 
}

take_screenshot

