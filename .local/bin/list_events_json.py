#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import caldav
from caldav import DAVClient
import datetime
import json
import os
import sys
import pytz
import logging

# Configure logging
logging.basicConfig(
    filename='/home/teraflops/list_events.log',
    level=logging.DEBUG,
    format='%(asctime)s:%(levelname)s:%(message)s'
)

def get_calendar_events():
    APPLE_ID = os.getenv('ICLOUD_APPLE_ID')
    APP_PASSWORD = os.getenv('ICLOUD_APP_PASSWORD')

    logging.debug(f"ICLOUD_APPLE_ID: {APPLE_ID}")
    logging.debug(f"ICLOUD_APP_PASSWORD: {APP_PASSWORD}")

    if not APPLE_ID or not APP_PASSWORD:
        logging.error("Credentials not set.")
        return "Credenciales no configuradas."

    try:
        client = DAVClient(
            url='https://caldav.icloud.com/',
            username=APPLE_ID,
            password=APP_PASSWORD,
#            verify_ssl=True  # Ensure SSL verification
        )
    except Exception as e:
        logging.exception("Failed to initialize DAVClient.")
        return f"Error al conectar con CalDAV: {e}"

    try:
        principal = client.principal()
        calendars = principal.calendars()
    except Exception as e:
        logging.exception("Failed to fetch calendars.")
        return f"Error al obtener calendarios: {e}"

    DESIRED_CALENDAR_NAME = "Trabajo"
    selected_calendar = None

    for calendar in calendars:
        if calendar.name.strip().lower() == DESIRED_CALENDAR_NAME.strip().lower():
            selected_calendar = calendar
            break

    if not selected_calendar:
        logging.error(f"Calendar '{DESIRED_CALENDAR_NAME}' not found.")
        return f"Calendario '{DESIRED_CALENDAR_NAME}' no encontrado."

    local_tz = pytz.timezone('Europe/Madrid')  # Replace with your timezone
    now = datetime.datetime.now(local_tz)
    in_7_days = now + datetime.timedelta(days=7)

    try:
        events = selected_calendar.date_search(start=now, end=in_7_days)
    except Exception as e:
        logging.exception("Failed to search for events.")
        return f"Error al buscar eventos: {e}"

    if not events:
        logging.info("No events found in the next 7 days.")
        return "No se encontraron eventos en los próximos 7 días."

    tooltip = ""
    for event in events:
        try:
            event_details = event.vobject_instance
            summary = getattr(event_details.vevent, 'summary', 'Sin Título').value
            dtstart = getattr(event_details.vevent, 'dtstart', 'Sin Hora de Inicio').value
            dtend = getattr(event_details.vevent, 'dtend', 'Sin Hora de Fin').value

            if isinstance(dtstart, datetime.datetime):
                dtstart = dtstart.astimezone(local_tz)
                dtstart_str = dtstart.strftime('%Y-%m-%d %H:%M')
            elif isinstance(dtstart, datetime.date):
                dtstart_str = dtstart.strftime('%Y-%m-%d') + " (Todo el Día)"
            else:
                dtstart_str = str(dtstart)

            if isinstance(dtend, datetime.datetime):
                dtend = dtend.astimezone(local_tz)
                dtend_str = dtend.strftime('%Y-%m-%d %H:%M')
            elif isinstance(dtend, datetime.date):
                dtend_str = dtend.strftime('%Y-%m-%d') + " (Todo el Día)"
            else:
                dtend_str = str(dtend)

            # Construct plain text tooltip
            tooltip += f"Evento: {summary}\nInicio: {dtstart_str}\nFin: {dtend_str}\n---\n"

        except Exception as e:
            logging.exception("Failed to parse an event.")
            continue

    return tooltip.strip('---\n')

def get_time():
    local_tz = pytz.timezone('Europe/Madrid')  # Replace with your timezone
    now = datetime.datetime.now(local_tz)
    return now.strftime('%a, %d %b %Y %H:%M')

if __name__ == "__main__":
    time_str = get_time()
    tooltip_str = get_calendar_events()

    output = {
        "text": time_str,
        "tooltip": tooltip_str
    }

    print(json.dumps(output, ensure_ascii=False))
    sys.exit(0)

