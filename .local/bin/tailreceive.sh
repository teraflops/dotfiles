#!/bin/bash

# this command has to be executed once you send to this computer a file through tailscale
# $1 is the path where the file will be put

sudo tailscale file get "$1"
