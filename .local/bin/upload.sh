#!/bin/bash

# Define the directory where the screenshot will be saved
#screenshot_dir="$HOME/Pictures/Screenshots"

# Create the directory if it doesn't exist
#mkdir -p "$screenshot_dir"

# Generate a unique filename based on date and time
#timestamp=$(date +"%Y-%m-%d-%H-%M-%S")
screenshot_file="$1"

# Capture the screen selection and save it to the specified file
#screencapture -i "$screenshot_file"
scp "$screenshot_file" teraflops@192.168.199.29:/usr/share/nginx/fiche/
# Copy the filename to the clipboard
paste=$(basename $screenshot_file)
echo -n "https://paste.priet.us/$paste" | wl-copy

# Notify the user
#echo "Screenshot saved as $screenshot_file and copied to the clipboard"
