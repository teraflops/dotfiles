#!/bin/bash
sinks=$(pw-dump | jq -r '.[] | select(.info.props."media.class" == "Audio/Sink") | "\(.info.props."node.name") \(.info.params.Format[].rate)"')

if [ -n "$sinks" ]; then
    # Extrae la tasa de muestreo del primer sink
    first_rate=$(echo "$sinks" | head -n 1 | awk '{print $2}')
    
    # Muestra todos los sinks y sus tasas de muestreo en el tooltip como texto plano
    tooltip=$(echo "$sinks" | sed 's/ / - /g' | tr '\n' ' | ' | sed 's/ | $//')

    # Imprime solo el JSON final
    echo "{\"text\": \"$first_rate Hz\", \"tooltip\": \"$tooltip\"}"
else
    echo "{\"text\": \"No sinks available\", \"tooltip\": \"No sinks detected\"}"
fi

