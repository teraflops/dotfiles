#!/bin/bash


rate=$(pw-dump|  jq '.[] | select(.info.props."media.class" == "Audio/Sink") | .info.params.Format[].rate')
[ -z "$rate" ] || echo $rate Hz 
