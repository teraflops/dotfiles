#!/bin/bash

# Define the array of colors
profiles=("Balanced" "Performance" "Quiet")

# Read the current value from the file
current_profile=$(</home/teraflops/.config/current_profile)

# Find the index of the current value in the array
index=-1
for i in "${!profiles[@]}"; do
    if [[ "${profiles[$i]}" == "$current_profile" ]]; then
        index=$i
        break
    fi
done

# Print the next value in the array
if (( index != -1 )); then
    next_index=$(( (index + 1) % ${#profiles[@]} ))
    next_profile=${profiles[$next_index]}
    echo "$next_profile" > ~/.config/current_profile
    asusctl profile -P "$next_profile"
else
    echo ""
fi

