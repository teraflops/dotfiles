# teraflops dotfiles

## custom waybar modules make use of kill signals to update the bar in real time avoidig polling and subsequend cpu usage

## Now playing and covers for Roon.

## Upload and download speed hidden in a tooltip.

## custom menus for bluetooth and wifi

## discrete integrated gpu indicator in real time (no polling)

## automated lyrics fetching and display in a tooltip


## Requirements

As this hyprland setup will work on any system, it is heavily focused on the rog zephyrus g14 2024 model.

## software

hyprland, hyprpaper, hypridle, hyprlock, waybar, foot terminal, gnome terminal, brightnessctl,
pamixer, rofi-lbonn-wayland-git, grim, slurp, wf-recorder, ffmpeg, wl-copy, howdy.

The computer has the g14 repo enabled and its tools and kernel installed as well.

https://asus-linux.org/gromptuides/arch-guide/

## change refresh rate from 120hz to 60hz when power cord is off on hyprland and restore to 120 when plugged in

### udev rules

```bash
#/etc/udev/rules.d/90-hyprland-onbattery.rules
SUBSYSTEM=="power_supply", KERNEL=="ACAD", ACTION=="change", ATTR{online}=="0", RUN+="/bin/runuser -u teraflops /usr/local/bin/hypr-monitor-switch.sh disconnected"

#/etc/udev/rules.d/91-hyprland-onpower.rules
SUBSYSTEM=="power_supply", KERNEL=="ACAD", ACTION=="change", ATTR{online}=="1", RUN+="/bin/runuser -u teraflops /usr/local/bin/hypr-monitor-switch.sh connected"
```

```bash
#!/bin/bash
# /usr/local/bin/hypr-monitor-switch.sh
read -r signature < /tmp/hyprland_instance_signature
export HYPRLAND_INSTANCE_SIGNATURE="$signature"
export XDG_RUNTIME_DIR="/run/user/1000"
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"
LOCKFILE="/tmp/hypr-monitor-switch.lock"

exec 200>$LOCKFILE
flock -n 200 || exit 1

if [[ "$1" == "disconnected" ]]; then
    hyprctl keyword monitor "eDP-1,2880x1800@60,0x0,1.5"
    echo "Set to 60Hz" >> /tmp/udev-hypr-monitor.log
elif [[ "$1" == "connected" ]]; then
    hyprctl keyword monitor "eDP-1,2880x1800@120,0x0,1.5,vrr,1,bitdepth,10"
    echo "Set to 120Hz" >> /tmp/udev-hypr-monitor.log
fi
```

you have to export the signature of the hyprland instance to the /tmp/hyprland_instance_signature file.

```bash
# ~/.config/hypr/conf/autostart.conf
exec-once = echo $HYPRLAND_INSTANCE_SIGNATURE > /tmp/hyprland_instance_signature
```

## [grab covers service](https://gitlab.com/teraflops/dotfiles/-/blob/main/.local/bin/mpdris2_cover.py?ref_type=heads) for local music if they are not available

mpDris2 is discouraged, use [mpdris2-rs](https://github.com/szclsya/mpdris2-rs) instead
